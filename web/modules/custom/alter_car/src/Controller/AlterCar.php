<?php

namespace Drupal\alter_car\Controller;

use Drupal\Core\Controller\ControllerBase;

/*
 * Class AlterCar.
 */
class AlterCar extends ControllerBase {

  /**
   * Get count user cars.
   */
  public function getCountUserCar() {
    $build = [
      'content' => [
        '#lazy_builder' => [
          'alter_car.lazy_builders:getCountCar', [],
        ],
        '#create_placeholder' => TRUE,
      ],
    ];

    return $build;
  }

}
