<?php

namespace Drupal\alter_car;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides #lazy_builder callbacks.
 */
class AlterCarLazyBuilders implements TrustedCallbackInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructs a new AlterCarLazyBuilders object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, AccountInterface $current_user) {
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $current_user;
  }

  /**
   * Builds the count car page.
   *
   * @return array
   *   A renderable array.
   */
  public function getCountCar() {
    $current_user_id = $this->currentUser->id();

    /* @var $user Drupal\user\UserInterface  */
    $user = $this->entityTypeManager->getStorage('user')->load($current_user_id);

    $count_cars = $user->field_car->count();

    return ['#markup' => 'The number of your cars = ' . $count_cars];
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['getCountCar'];
  }

}
