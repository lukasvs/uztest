(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.colorField = {
    attach: function (context, settings) {
      $('.cars__field-color', context).once('colorField').each(function () {
        let color_name = $(this).text();
        $(this).css({'background-color': color_name});
      });
    }
  };

})(jQuery, Drupal);
